export default {
  "touchiconMimeType": "image/png",
  "faviconMimeType": "image/png",
  "precomposed": false,
  "touchicons": [
    {
      "width": 76,
      "src": "/assets/static/src/favicon.png?width=76&key=e10953e"
    },
    {
      "width": 152,
      "src": "/assets/static/src/favicon.png?width=152&key=e10953e"
    },
    {
      "width": 120,
      "src": "/assets/static/src/favicon.png?width=120&key=e10953e"
    },
    {
      "width": 167,
      "src": "/assets/static/src/favicon.png?width=167&key=e10953e"
    },
    {
      "width": 180,
      "src": "/assets/static/src/favicon.png?width=180&key=e10953e"
    }
  ],
  "favicons": [
    {
      "width": 16,
      "src": "/assets/static/src/favicon.png?width=16&key=cf96b08"
    },
    {
      "width": 32,
      "src": "/assets/static/src/favicon.png?width=32&key=cf96b08"
    },
    {
      "width": 96,
      "src": "/assets/static/src/favicon.png?width=96&key=cf96b08"
    }
  ]
}